# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from .consumers import HelloEventsConsumer
from logging.config import fileConfig
from minty.cqrs import CQRS
from minty.infrastructure import InfrastructureFactory
from minty.middleware import AmqpPublisherMiddleware
from minty_amqp.client import AMQPClient
from minty_infra_sqlalchemy import DatabaseTransactionMiddleware
from zsnl_domains import hello

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = "{{ cookiecutter.package_name }}"
    return record


def main():
    fileConfig("logging.conf")

    logging.setLogRecordFactory(log_record_factory)

    infra_factory = InfrastructureFactory(config_file="config.conf")
    cqrs = CQRS(
        domains=[hello],
        infrastructure_factory=infra_factory,
        command_wrapper_middleware=[
            DatabaseTransactionMiddleware("database"),
            AmqpPublisherMiddleware(
                publisher_name="hello", infrastructure_name="amqp"
            ),
        ],
    )

    config = cqrs.infrastructure_factory.get_config(context=None)
    amqp_client = AMQPClient(config, cqrs=cqrs)
    amqp_client.register_consumers([HelloEventsConsumer])
    amqp_client.start()


def init():
    if __name__ == "__main__":
        main()


init()
