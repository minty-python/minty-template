# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty import Base


class BaseHandler(Base):
    def __init__(self, cqrs):
        self.cqrs = None
        self.routing_keys = []

    def get_command_instance(self, event):
        return self.cqrs.get_command_instance(
            event.correlation_id,
            "zsnl_domains.hello",
            event.context,
            event.user_uuid,
        )


class HelloHandler(BaseHandler):
    def __init__(self, cqrs):
        self.routing_keys = ["zsnl.v2.zsnl_domains_hello.Hello.Hello"]
        self.cqrs = cqrs

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.say_hello(case_uuid=event.entity_id, changes=changes)
