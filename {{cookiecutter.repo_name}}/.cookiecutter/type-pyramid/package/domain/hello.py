# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


"""Hello World demonstration domain.

Please remove this when you start working on your application.
"""

import os
import typing
from pkgutil import get_data
from uuid import UUID

from minty import Base
from minty.cqrs import event
from minty.validation import validate_with


class HelloInstance(typing.NamedTuple):
    uuid: UUID
    name: str


class HelloInfrastructure(Base):
    """Infrastructure class to save "Hello" messages to files"""

    def __init__(self, path):
        self.path = path

    def _get_storage_filename(self, uuid):
        return os.path.join(self.path, uuid + ".txt")

    def save(self, uuid, name):
        path = self._get_storage_filename(uuid)

        with open(path, "w", encoding="utf-8") as f:
            f.write(name)

        return

    def load(self, uuid):
        path = self._get_storage_filename(uuid)

        with open(path, "r", encoding="utf-8") as f:
            name = f.readline()

        return name


def hello_infrastructure(config):
    return HelloInfrastructure(config.get("hello_path", "/tmp"))


class HelloRepository(Base):
    REQUIRED_INFRASTRUCTURE = {"hello_infrastructure": hello_infrastructure}

    def __init__(self, context, infrastructure_factory):
        self.context = context
        self.infra_factory = infrastructure_factory

    def set_name(self, new_name: HelloInstance):
        self.logger.info(self.infra_factory.registered_infrastructure)

        hello_infra = self.infra_factory.get_infrastructure(
            self.context, "hello_infrastructure"
        )

        hello_infra.save(new_name.uuid, new_name.name)
        return

    def get_name(self, uuid: UUID):
        hello_infra = self.infra_factory.get_infrastructure(
            self.context, "hello_infrastructure"
        )

        name = hello_infra.load(uuid)

        return HelloInstance(uuid, name)


class HelloQuery(Base):
    """Respond to 'Hello World' queries with a greeting"""

    def __init__(self, repository_factory, context):
        self.repository_factory = repository_factory
        self.context = context

    def hello_query(self, uuid):
        """Return a name-specific greeting"""
        repo = self.repository_factory.get_repository(
            context=self.context, name="HelloRepository"
        )

        hello = repo.get_name(uuid=uuid)

        return f"Hello, {hello.name} from {self.context}"


class HelloCommand(Base):
    """Handle the basic "Hello" command"""

    def __init__(self, repository_factory, context):
        self.repository_factory = repository_factory
        self.context = context

    @validate_with(get_data(__name__, "set_hello_name.json"))
    @event("HelloNameSet")
    def set_hello_name(self, uuid, name):
        new_name = HelloInstance(uuid=uuid, name=name)

        repo = self.repository_factory.get_repository(
            context=self.context, name="HelloRepository"
        )

        repo.set_name(new_name)
        return


def get_query_instance(repo_factory, context):
    """Return a HelloQuery instance, ready to be queried"""
    return HelloQuery(repo_factory, context)


def get_command_instance(repo_factory, context):
    return HelloCommand(repo_factory, context)


REQUIRED_REPOSITORIES = {"HelloRepository": HelloRepository}
