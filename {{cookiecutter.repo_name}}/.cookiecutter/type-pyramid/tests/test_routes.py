# AUTO GENERATED FILE - don't edit!
# Re-create by running 'generate-routes --package-name={package_name}'
# Run 'generate-routes --help' for more options


from unittest import TestCase, mock

import webtest
from {{ cookiecutter.package_name }} import main


class TestRoutesHello(TestCase):
    def setUp(self):
        from {{ cookiecutter.package_name }}.views import hello

        self.patched_hello = mock.patch(
            "{{ cookiecutter.package_name }}.views.hello", spec=hello
        )
        self.hello = self.patched_hello.start()

        settings = {
            "minty_service.infrastructure.config_file": "tests/data/config.conf"
        }
        app = main({}, **settings)
        self.mock_app = webtest.TestApp(app)

    def tearDown(self):
        self.patched_hello.stop()

    def test_routes_hello(self):
        self.hello.hello_world.return_value = 200
        self.mock_app.get("/hello", status=200)
        self.hello.hello_save.return_value = 200
        self.mock_app.get("/set_hello", status=200)
